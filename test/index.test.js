import { shallow } from "enzyme";
import React from "react";
import { Provider } from "react-redux";

import api from "../src/api";
import { Favourite, Favourites, NewList } from "../src/components";
import store from "../src/store";

const withRedux = (Component) => (
  <Provider store={store}>
    <Component />
  </Provider>
);

describe("integration", () => {
  describe("api", () => {
    it("should return a list of 'favorites' type", async () => {
      const lists = await api.lists();
      const actual = lists["1"].type;
      const expected = "favorites";

      expect(actual).toBe(expected);
    });

    it("should create a list", async () => {
      const { message } = await api.newList({ label: "hello" });
      const expected = "list successfully created";

      expect(message).toBe(expected);
    });
  });

  describe("app", () => {
    // it("should submit the new list", (done) => {
    //   const _handleSubmit = () => {
    //     return Promise.resolve(wrapper.state().listLabel);
    //   };
    //   const wrapper = shallow(
    //     withRedux(<NewList handleSubmit={_handleSubmit} />)
    //   );
    //   wrapper.setState({ creatingList: true, listLabel: "my list" });
    //   wrapper.find("button").simulate("click");
    //   setImmediate(() => {
    //     const actual = wrapper.state().submitResponse;
    //     expect(actual).toBe("success");
    //     done();
    //   });
    // });
    // it("should handle an error in case submit fails", (done) => {
    //   const _handleSubmit = () => {
    //     throw new Error("nope");
    //   };
    //   const wrapper = shallow(
    //     withRedux(<NewList handleSubmit={_handleSubmit} />)
    //   );
    //   wrapper.setState({ creatingList: true, listLabel: "my list" });
    //   wrapper.find("button").simulate("click");
    //   setImmediate(() => {
    //     const actual = wrapper.state().submitResponse;
    //     expect(actual).toBe("nope");
    //     done();
    //   });
    // });
    // it("should clean the state if the submit was successful", (done) => {
    //   const _handleSubmit = () => {
    //     return Promise.resolve(wrapper.state().listLabel);
    //   };
    //   const wrapper = shallow(
    //     withRedux(<NewList handleSubmit={_handleSubmit} />)
    //   );
    //   wrapper.setState({ creatingList: true, listLabel: "my list" });
    //   wrapper.find("button").simulate("click");
    //   setImmediate(() => {
    //     const { creatingList, listLabel } = wrapper.state();
    //     expect(creatingList).toBe(false);
    //     expect(listLabel).toBe("");
    //     done();
    //   });
    // });
  });
});

describe("units", () => {
  it("should have a 'custom' css class", () => {
    const list = { type: "custom", businesses: { "1": {} } };
    const wrapper = shallow(<Favourite list={list} />);

    expect(wrapper.find(".custom").length).toBe(1);
  });

  it("should have a css class for the 'favorites' list", () => {
    const list = { type: "favorites", businesses: { "1": {} } };
    const wrapper = shallow(<Favourite list={list} />);

    expect(wrapper.find(".favorites").length).toBe(1);
  });

  // it("should display the name of the list", () => {
  //   const list = { type: "a", label: "label_1", businesses: { "1": {} } };
  //   const wrapper = shallow(<Favourite list={list} />);
  //   const actual = wrapper
  //     .find("p")
  //     .first()
  //     .text();
  //   const expected = "name: label_1";

  //   expect(actual).toBe(expected);
  // });

  it("should render a list of favourites", () => {
    const business_1 = { "1": {} };
    const business_2 = { "1": {}, "2": {} };
    const lists = {
      "1": { type: "a", label: "a", businesses: business_1 },
      "2": { type: "a", label: "b", businesses: business_2 }
    };

    const wrapper = shallow(<Favourites lists={lists} />);

    expect(wrapper.children().length).toBe(1);
  });

  // it("should display a text input for the label of the list", () => {
  //   const wrapper = shallow(withRedux(<NewList />));
  //   wrapper.find("button").simulate("click");

  //   expect(wrapper.find("input").length).toBe(1);
  // });

  // it("should set the label of the list", () => {
  //   const expected = "hello";
  //   const wrapper = shallow(withRedux(<NewList />));

  //   wrapper.setState({ creatingList: true });
  //   wrapper.find("input").simulate("change", { target: { value: expected } });

  //   const actual = wrapper.state().listLabel;

  //   expect(actual).toBe(expected);
  // });

  // it("should render a submit button", () => {
  //   const wrapper = shallow(withRedux(<NewList />));
  //   wrapper.setState({ creatingList: true });

  //   expect(wrapper.find("button").text()).toBe("submit");
  // });

  // it("should disable the submit button if no label was given", () => {
  //   const wrapper = shallow(withRedux(<NewList />));
  //   wrapper.setState({ creatingList: true });

  //   expect(wrapper.find("button").prop("disabled")).toBe(true);

  //   wrapper
  //     .find("input")
  //     .simulate("change", { target: { value: "any valid value" } });

  //   expect(wrapper.find("button").prop("disabled")).toBe(false);
  // });
});
