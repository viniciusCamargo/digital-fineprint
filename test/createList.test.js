import { shallow } from "enzyme";
import React from "react";

import { Favourites } from "../src/components";

describe("new list", () => {
  it("should create a new list", () => {
    const business_1 = { "1": {} };
    const business_2 = { "1": {}, "2": {} };
    const lists = {
      "1": { type: "a", label: "a", businesses: business_1 },
      "2": { type: "a", label: "b", businesses: business_2 }
    };

    const wrapper = shallow(<Favourites lists={lists} />);

    expect(wrapper.children().length).toBe(1);
  });
});
