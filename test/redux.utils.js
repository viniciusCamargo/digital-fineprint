import React from "react";
import { createStore } from "redux";
import { Provider } from "react-redux";

const initialState = {
  lists: {},
  error: "",
  responseMessage: "",
  lastIdAdded: 3, // to match the api
  listId: 0,
  listLabel: ""
};

function counter(state = { value: 0 }, action) {
  switch (action.type) {
    case "INCREMENT":
      return { value: state.value + 1 };
    case "DECREMENT":
      return { value: state.value - 1 };
    default:
      return state;
  }
}

export const store = createStore(
  counter,
  initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export function injectRedux(App) {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
}
