module.exports = {
  presets: [
    "@babel/preset-react",
    process.env.NODE_ENV === "test"
      ? ["@babel/preset-env", { targets: { node: "current" } }]
      : ["@babel/preset-env"]
  ],
  plugins: [
    "@babel/plugin-proposal-object-rest-spread",
    "@babel/plugin-proposal-class-properties",
    "@babel/plugin-transform-classes"
  ]
};
