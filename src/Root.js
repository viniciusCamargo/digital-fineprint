import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Container } from "semantic-ui-react";

import { Home, Businesses, NewBusiness } from "./pages";

export default () => (
  <Container>
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/business" exact component={Businesses} />
        <Route path="/business/:listId" exact component={NewBusiness} />

        <Route component={() => <pre>404</pre>} />
      </Switch>
    </Router>
  </Container>
);
