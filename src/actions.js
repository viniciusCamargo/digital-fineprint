import api from "./api";

export const getLists = () => async (dispatch) => {
  dispatch({ type: "FETCHING", fetching: true });

  try {
    const lists = await api.lists();

    dispatch({ type: "ADD_LISTS", lists });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  } finally {
    dispatch({ type: "FETCHING", fetching: false });
  }
};

export const getList = (listId) => async (dispatch) => {
  dispatch({ type: "FETCHING", fetching: true });

  try {
    const list = await api.list(listId);

    dispatch({ type: "SET_CURRENT_LIST", list });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  } finally {
    dispatch({ type: "FETCHING", fetching: false });
  }
};

function generateList({ label, id }) {
  return {
    [id]: { id, type: "custom", label, businesses: {} }
  };
}

export const createList = ({ label }) => async (dispatch, ownProps) => {
  try {
    const { message } = await api.newList({ label });

    const { lastIdAdded } = ownProps();
    const newListId = lastIdAdded + 1;

    dispatch({ type: "API_RESPONSE_MESSAGE", message });
    dispatch({
      type: "ADD_TO_LISTS",
      newLists: generateList({ label, id: newListId })
    });
    dispatch({ type: "LAST_ID_ADDED", listId: newListId });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  }
};

export const deleteList = (listId) => async (dispatch, ownProps) => {
  try {
    const { message } = await api.deleteList(listId);

    if (ownProps().listId === listId) {
      dispatch({ type: "CLEAR_LIST_BEING_RENAMED" });
    }

    dispatch({ type: "API_RESPONSE_MESSAGE", message });
    dispatch({ type: "REMOVE_FROM_LISTS", listId });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  }
};

export const clearListBeingRenamed = () => (dispatch) => {
  dispatch({ type: "CLEAR_LIST_BEING_RENAMED" });
};

export const setListBeingRenamed = (listId, listLabel) => (dispatch) => {
  dispatch({ type: "LIST_BEING_RENAMED", payload: { listId, listLabel } });
};

export const setNewLabel = (newLabel) => (dispatch) => {
  dispatch({ type: "NEW_LIST_LABEL", newLabel });
};

export const renameList = ({ listId, newLabel }) => async (dispatch) => {
  try {
    const { message } = await api.renameList({ listId, newLabel });

    if (message) {
      throw new Error(message);
    }

    dispatch({ type: "API_RESPONSE_MESSAGE", message: "list updated" });
    dispatch({ type: "RENAME_LIST", payload: { listId, newLabel } });
    dispatch({ type: "CLEAR_LIST_BEING_RENAMED" });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  }
};

export const getBusinesses = () => async (dispatch) => {
  try {
    const businesses = await api.getBusinesses();

    dispatch({ type: "SET_BUSINESSES", businesses });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  }
};

export const createBusiness = (params) => async (dispatch) => {
  try {
    const { listId, name, postcode, registrationNumber } = params;
    const businesses = await api.newBusiness({
      listId,
      name,
      postcode,
      registrationNumber
    });

    dispatch({ type: "SET_BUSINESSES", businesses });
  } catch (error) {
    dispatch({ type: "SET_ERROR_MESSAGE", error: error.message });
  }
};

export const clearResponseMessage = () => async (dispatch) => {
  dispatch({ type: "API_RESPONSE_MESSAGE", message: "" });
};

export const clearErrorMessage = () => async (dispatch) => {
  dispatch({ type: "SET_ERROR_MESSAGE", error: "" });
};

export const setCurrentList = (list) => (dispatch) => {
  dispatch({ type: "SET_CURRENT_LIST", list });
};
