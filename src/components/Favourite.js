import React from "react";
import { Card, Icon } from "semantic-ui-react";

import DeleteButton from "./DeleteButton";
import RenameButton from "./RenameButton";
import NewBusinessButton from "./NewBusinessButton";

function Favourite({ list }) {
  const styleClasses = `list-item ${
    list.type === "favorites" ? "favorites" : "custom"
  }`;

  const numberOfBusinesss = Object.entries(list.businesses).length;

  return (
    <Card className={styleClasses}>
      <Card.Content>
        <Card.Header>
          {list.label}{" "}
          {list.type === "favorites" ? (
            <Icon name="heart outline" size="small" color="red" margin={10} />
          ) : null}
        </Card.Header>

        {numberOfBusinesss ? (
          <div>
            {numberOfBusinesss}
            {numberOfBusinesss > 1 ? " businesses" : " business"}
          </div>
        ) : null}
      </Card.Content>

      <Card.Content extra>
        <div className="ui three buttons">
          <DeleteButton listId={list.id} />
          <RenameButton listId={list.id} />
          <NewBusinessButton listId={list.id} />
        </div>
      </Card.Content>
    </Card>
  );
}

export default Favourite;
