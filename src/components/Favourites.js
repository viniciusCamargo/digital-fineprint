import React from "react";
import { Card } from "semantic-ui-react";

import Favourite from "./Favourite";

function Favourites({ lists }) {
  const ListsKeys = Object.keys(lists);

  return (
    <Card.Group>
      {ListsKeys.length
        ? ListsKeys.map((id) => {
            const list = lists[id];

            return <Favourite key={id} list={list} />;
          })
        : null}
    </Card.Group>
  );
}

export default Favourites;
