import React from "react";
import { Card } from "semantic-ui-react";

function CardBusiness(props) {
  return (
    <Card>
      <Card.Content>
        <Card.Header>{props.name}</Card.Header>
      </Card.Content>

      <Card.Content extra>
        <div>
          <strong>Postcode:</strong> {props.postcode}
        </div>

        <div>
          <strong>Registration Number:</strong> {props.registration_number}
        </div>
      </Card.Content>
    </Card>
  );
}

export default CardBusiness;
