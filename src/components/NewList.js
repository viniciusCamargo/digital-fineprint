import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";

import { createList } from "../actions";
import Editor from "./Editor";
import Padding from "./Padding";

class NewList extends React.Component {
  render() {
    return (
      <React.Fragment>
        {this.state.creatingList && !this.props.listId && (
          <Editor
            label="list name"
            onCancel={() => this.setState({ creatingList: false })}
            onChange={this._handleChange}
            onSubmit={this._handleCreate}
            disabled={!this.state.label}
            buttonText="submit"
          />
        )}

        {!this.state.creatingList && !this.props.listId && (
          <Padding style={{ paddingTop: "2em" }}>
            <Button
              basic
              color="green"
              onClick={() => this.setState({ creatingList: true })}>
              create new list
            </Button>

            <Link to="/business">visit businesses</Link>
          </Padding>
        )}
      </React.Fragment>
    );
  }

  _handleCreate = async () => {
    const { label } = this.state;

    await this.props.createList({ label });

    this.setState({
      creatingList: false,
      label: ""
    });
  };

  _handleChange = ({ target: { value } }) => {
    this.setState({ label: value.trim() });
  };

  state = {
    creatingList: false,
    label: ""
  };
}

const mapDispatchToProps = { createList };

export default connect(
  ({ listId }) => ({ listId }),
  mapDispatchToProps
)(NewList);
