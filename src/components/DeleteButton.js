import React from "react";
import { connect } from "react-redux";
import { Button } from "semantic-ui-react";

import { deleteList } from "../actions";

function DeleteButton(props) {
  const _handleClick = async () => await props.deleteList(props.listId);

  return (
    <Button basic color="red" onClick={_handleClick}>
      delete
    </Button>
  );
}

export default connect(
  null,
  { deleteList }
)(DeleteButton);
