import React from "react";
import { connect } from "react-redux";
import { Button } from "semantic-ui-react";

import { setListBeingRenamed } from "../actions";

function RenameButton(props) {
  const _handleClick = async () => {
    window.scrollTo(0, 0);
    const { label } = props.lists[props.listId];

    props.setListBeingRenamed(props.listId, label);
  };

  return (
    <Button basic color="blue" onClick={_handleClick}>
      rename
    </Button>
  );
}

export default connect(
  ({ lists }) => ({ lists }),
  { setListBeingRenamed }
)(RenameButton);
