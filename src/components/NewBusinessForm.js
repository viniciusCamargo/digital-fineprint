import React from "react";
import { Button, Form, Grid } from "semantic-ui-react";

const formFields = [
  ["postcode", "postcode"],
  ["registrationNumber", "registration number"],
  ["name", "name"]
];

class NewBusinessForm extends React.Component {
  render() {
    return (
      <Form onSubmit={this._handleSubmit}>
        {formFields.map(([key, placeholder]) => (
          <Form.Field key={key}>
            <label>
              {placeholder}
              <input
                placeholder={placeholder}
                value={this.state[key]}
                onChange={({ target }) => {
                  this.setState({ [key]: target.value });
                }}
              />
            </label>
          </Form.Field>
        ))}

        <Button positive type="submit" disabled={this._isDisabled()}>
          create
        </Button>
      </Form>
    );
  }

  _handleSubmit = (e) => {
    e.preventDefault();

    this.props.onSubmit(this.state);
  };

  _isDisabled = () => {
    const { postcode, registrationNumber, name } = this.state;

    return !(postcode && registrationNumber && name);
  };

  state = {
    postcode: "",
    registrationNumber: "",
    name: ""
  };
}

export default ({ children }) => (
  <Grid container columns={2} stackable>
    <Grid.Column>
      <NewBusinessForm />
    </Grid.Column>
    <Grid.Column>{children}</Grid.Column>
  </Grid>
);
