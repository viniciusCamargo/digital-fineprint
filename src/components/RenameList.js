import React from "react";
import { connect } from "react-redux";

import { clearListBeingRenamed, renameList, setNewLabel } from "../actions";
import Editor from "./Editor";

class NewList extends React.Component {
  render() {
    return this.props.listId ? (
      <Editor
        buttonText="submit"
        disabled={!this.props.listLabel}
        label="new list name"
        onCancel={this._handleCancel}
        onChange={this._handleChange}
        onSubmit={this._handleRename}
        value={this.props.listLabel}
      />
    ) : null;
  }

  _handleChange = (e) => {
    this.props.setNewLabel(e.target.value);
  };

  _handleCancel = () => {
    this.props.clearListBeingRenamed();
  };

  _handleRename = async () => {
    await this.props.renameList({
      listId: this.props.listId,
      newLabel: this.props.listLabel.trim()
    });
  };
}

const mapStateToProps = (state) => ({
  listId: state.listId,
  listLabel: state.listLabel
});

const mapDispatchToProps = {
  clearListBeingRenamed,
  setNewLabel,
  renameList
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewList);
