import React from "react";

function Padding({ children, margin = "2em 0", style }) {
  return children ? <div style={{ margin, ...style }}>{children}</div> : null;
}

export default Padding;
