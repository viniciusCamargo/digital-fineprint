import React from "react";
import { connect } from "react-redux";
import { Message } from "semantic-ui-react";

import { Fade } from "../components";

function Notifications({ error, responseMessage }) {
  return (
    <React.Fragment>
      {error && (
        <Fade>
          <Message error header={error} />
        </Fade>
      )}

      {responseMessage && (
        <Fade>
          <Message success header={responseMessage} />
        </Fade>
      )}
    </React.Fragment>
  );
}

const mapStateToProps = ({ error, responseMessage }) => ({
  error,
  responseMessage
});

export default connect(mapStateToProps)(Notifications);
