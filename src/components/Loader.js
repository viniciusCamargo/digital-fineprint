import React from "react";
import { connect } from "react-redux";
import { Dimmer, Loader as SemuiLoader } from "semantic-ui-react";

function Loader({ isFetching }) {
  return isFetching ? (
    <Dimmer active inverted>
      <SemuiLoader inverted content="Loading" />
    </Dimmer>
  ) : null;
}

const mapStateToProps = ({ isFetching }) => ({ isFetching });

export default connect(mapStateToProps)(Loader);
