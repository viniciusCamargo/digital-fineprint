import React from "react";
import { connect } from "react-redux";

import { clearErrorMessage, clearResponseMessage } from "../actions";

class Timeout extends React.Component {
  render() {
    return this.state.hide ? null : this.props.children;
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ hide: true });

      if (this.props.responseMessage) {
        this.props.clearResponseMessage();
      }

      if (this.props.error) {
        this.props.clearErrorMessage();
      }
    }, 2500);
  }

  state = { hide: false };
}

const mapStateToProps = ({ responseMessage, error }) => ({
  responseMessage,
  error
});

const mapDispatchToProps = { clearResponseMessage, clearErrorMessage };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Timeout);
