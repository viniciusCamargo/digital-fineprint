import React from "react";
import { connect } from "react-redux";

import { createBusiness } from "../actions";

class NewBusiness extends React.Component {
  render() {
    return (
      <React.Fragment>
        {this.state.creatingBusiness ? (
          this._renderForm()
        ) : (
          <button onClick={() => this.setState({ creatingBusiness: true })}>
            create business
          </button>
        )}
      </React.Fragment>
    );
  }

  _clearForm = () => {
    this.setState({
      name: "",
      postcode: "",
      registrationNumber: ""
    });
  };

  _renderForm = () => (
    <React.Fragment>
      {Object.entries({
        name: "name",
        postcode: "postcode",
        registrationNumber: "registration number"
      }).map(([key, value]) => (
        <label key={key}>
          {value}

          <input
            type="text"
            onChange={({ target: { value } }) => {
              this.setState({ [key]: value });
            }}
            value={this.state[key]}
          />
        </label>
      ))}

      <button onClick={this._handleClick}>submit</button>
    </React.Fragment>
  );

  _handleClick = async () => {
    const { name, postcode, registrationNumber } = this.state;

    await this.props.createBusiness({
      listId: this.props.listId,
      name,
      postcode,
      registrationNumber
    });
  };

  state = {
    creatingBusiness: false,
    name: "",
    postcode: "",
    registrationNumber: ""
  };
}

const mapDispatchToProps = { createBusiness };

export default connect(
  null,
  mapDispatchToProps
)(NewBusiness);
