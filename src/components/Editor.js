import React from "react";
import { Button, Form, Input } from "semantic-ui-react";

import Padding from "./Padding";

function Editor({
  buttonText,
  disabled,
  onCancel,
  onChange,
  onSubmit,
  label,
  value
}) {
  return (
    <Padding>
      <Form onSubmit={onSubmit}>
        <Form.Field>
          <label>{label}</label>
          <Input type="text" onChange={onChange} value={value} />
        </Form.Field>

        <Button type="submit" basic color="green" disabled={disabled}>
          {buttonText}
        </Button>

        <Button type="button" basic color="red" onClick={onCancel}>
          cancel
        </Button>
      </Form>
    </Padding>
  );
}

export default Editor;
