import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Button } from "semantic-ui-react";

import { setCurrentList } from "../actions";

function NewBusinessButton({ history, list, listId, setCurrentList }) {
  return (
    <Button
      basic
      color="green"
      as="a"
      href={`/business/${listId}`}
      onClick={(e) => {
        e.preventDefault();

        setCurrentList(list);

        history.push(`/business/${listId}`);
      }}>
      new business
    </Button>
  );
}

const mapStateToProps = ({ lists }, ownProps) => ({
  list: lists[ownProps.listId]
});

export default connect(
  mapStateToProps,
  { setCurrentList }
)(withRouter(NewBusinessButton));
