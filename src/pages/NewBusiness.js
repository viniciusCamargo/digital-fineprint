import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { format } from "date-fns";

import { Icon, Item } from "semantic-ui-react";

import { getList } from "../actions";
import { Loader, NewBusinessForm, Padding } from "../components";

class NewBusiness extends React.Component {
  render() {
    const { created_at, label, type } = this.props.list;
    const icon =
      type === "favorites" ? (
        <Icon name="heart outline" size="small" color="red" />
      ) : null;

    return (
      <Padding>
        <Loader />

        <Link to="/">return to lists</Link>

        <Item.Group>
          <Item>
            <Item.Content>
              {this.state.listId === "new" ? (
                <Item.Header>new business</Item.Header>
              ) : (
                <React.Fragment>
                  <Item.Header>
                    {label} {icon}
                  </Item.Header>

                  <Item.Meta>
                    created at: {format(created_at, ["D of MMMM, YYYY"])}
                  </Item.Meta>
                </React.Fragment>
              )}
            </Item.Content>
          </Item>
        </Item.Group>

        <NewBusinessForm />
      </Padding>
    );
  }

  async componentDidMount() {
    document.title = "new business - digital fineprint";

    this._retrieveOrFetchList();
  }

  _retrieveOrFetchList = async () => {
    const {
      match: {
        params: { listId }
      }
    } = this.props;

    this.setState({ listId });

    if (!Object.entries(this.props.list).length && listId !== "new") {
      /**
       * if the user is coming from a full refresh the redux store
       * will not be populated with the list we are trying to display
       * so we will fetch it using its 'id'
       */

      this.props.getList(listId);
    }
  };

  state = { listId: 0 };
}

const mapStateToProps = ({ error, isFetching, list, responseMessage }) => ({
  error,
  isFetching,
  list,
  responseMessage
});

const mapDispatchToProps = { getList };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewBusiness);
