import React from "react";
import { connect } from "react-redux";

import { getLists } from "../actions";
import {
  Favourites,
  Loader,
  NewList,
  Notifications,
  Padding,
  RenameList
} from "../components";

class App extends React.Component {
  render() {
    const { lists } = this.props;

    return (
      <React.Fragment>
        <Padding>
          <Notifications />
          <Loader />
        </Padding>

        <NewList />
        <RenameList />
        <Favourites lists={lists} />
      </React.Fragment>
    );
  }

  async componentDidMount() {
    document.title = "home - digital fineprint";

    await this.props.getLists();
  }
}

export default connect(
  ({ lists }) => ({ lists }),
  { getLists }
)(App);
