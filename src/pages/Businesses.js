import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { Button, Card } from "semantic-ui-react";

import { createBusiness, getBusinesses } from "../actions";
import {
  CardBusiness,
  NewBusinessForm,
  Notifications,
  Padding
} from "../components";

class Businesses extends React.Component {
  render() {
    const { businesses } = this.props;
    const businessesIds = businesses ? Object.keys(businesses) : [];

    return (
      <Padding>
        <Notifications />

        <Padding style={{ paddingTop: "2em" }}>
          <NewBusinessButton />

          <Link to="/">return to lists</Link>
        </Padding>

        <Card.Group>
          {businessesIds.map((businessId) => {
            const { id, data } = businesses[businessId];

            return <CardBusiness key={id} {...data} />;
          })}
        </Card.Group>
      </Padding>
    );
  }

  async componentDidMount() {
    document.title = "businesses - digital fineprint";

    await this.props.getBusinesses();
  }
}

const NewBusinessButton = withRouter(({ history, listId }) => (
  <Button
    basic
    color="green"
    onClick={(e) => {
      if (listId) {
        history.push(`/business/${listId}`);
      } else {
        history.push(`/business/new`);
      }
    }}>
    create new business
  </Button>
));

const mapStateToProps = ({ businesses }) => ({
  businesses
});

const mapDispatchToProps = { getBusinesses, createBusiness };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Businesses);
