import { applyMiddleware, compose as reduxCompose, createStore } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";

import reducer from "./reducer";

const middleware = [thunk];

const initialState = {
  error: "",
  lastIdAdded: 3, // to match the api
  list: {},
  listId: 0,
  listLabel: "",
  lists: {},
  responseMessage: ""
};

if (process.env.NODE_ENV !== "production") {
  middleware.push(logger);
}

const compose =
  typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : reduxCompose;

const storeEnhancer = compose(applyMiddleware(...middleware));

export default createStore(reducer, initialState, storeEnhancer);
