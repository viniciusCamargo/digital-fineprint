import generateReandomId from "nanoid";

// const basePath = "https://young-falls-32172.herokuapp.com/api";
const basePath = "http://localhost:3000/api";

async function lists() {
  const response = await fetch(`${basePath}/list`);

  return await response.json();
}

async function list(listId) {
  const response = await fetch(`${basePath}/list/${listId}`);

  return await response.json();
}

async function newList({ label }) {
  const options = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ label })
  };
  const request = await fetch(`${basePath}/list`, options);

  return await request.json();
}

async function deleteList(listId) {
  const options = { method: "DELETE" };
  const request = await fetch(`${basePath}/list/${listId}`, options);

  return await request.json();
}

async function renameList({ listId, newLabel }) {
  const options = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ label: newLabel })
  };
  const request = await fetch(`${basePath}/list/${listId}`, options);

  return await request.json();
}

async function getBusinesses() {
  const response = await fetch(`${basePath}/business`);

  return await response.json();
}

async function newBusiness({ listId, name, postcode, registrationNumber }) {
  const options = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ name, postcode, registrationNumber })
  };
  const request = await fetch(
    `${basePath}/list/${listId}/business/${generateReandomId()}`,
    options
  );

  return await request.json();
}

export default {
  lists,
  list,
  newList,
  deleteList,
  renameList,
  getBusinesses,
  newBusiness
};
