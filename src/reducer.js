function removeList(stateLists, listId) {
  return Object.keys(stateLists).reduce((lists, id) => {
    if (id !== String(listId)) {
      lists[id] = stateLists[id];
    }

    return lists;
  }, {});
}

function renameList(stateLists, listId, newLabel) {
  const renamedList = Object.keys(stateLists).reduce((lists, id) => {
    if (id === String(listId)) {
      lists[listId] = { ...stateLists[listId], label: newLabel };
    }

    return lists;
  }, {});

  return { ...stateLists, ...renamedList };
}

function reducer(state, action) {
  switch (action.type) {
    case "ADD_LISTS":
      return { ...state, lists: action.lists };

    case "ADD_TO_LISTS":
      return { ...state, lists: { ...state.lists, ...action.newLists } };

    case "API_RESPONSE_MESSAGE":
      return { ...state, responseMessage: action.message };

    case "CLEAR_LIST_BEING_RENAMED":
      return { ...state, listId: 0, listLabel: "" };

    case "FETCHING":
      return { ...state, isFetching: action.fetching };

    case "LAST_ID_ADDED":
      return { ...state, lastIdAdded: action.listId };

    case "LIST_BEING_RENAMED":
      return {
        ...state,
        listId: action.payload.listId,
        listLabel: action.payload.listLabel
      };

    case "NEW_LIST_LABEL":
      return { ...state, listLabel: action.newLabel };

    case "REMOVE_FROM_LISTS":
      return { ...state, lists: removeList(state.lists, action.listId) };

    case "RENAME_LIST":
      const { listId, newLabel } = action.payload;
      return { ...state, lists: renameList(state.lists, listId, newLabel) };

    case "SET_BUSINESSES":
      return { ...state, businesses: action.businesses };

    case "SET_CURRENT_LIST":
      return { ...state, list: action.list };

    case "SET_ERROR_MESSAGE":
      return { ...state, error: action.error };

    default:
      return state;
  }
}

export default reducer;
